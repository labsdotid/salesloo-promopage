<?php

function salesloo_promopage_get($api_path, $args = [])
{
    $base = rtrim(salesloo_get_option('affiliate_promopage_base'), '/');
    $url = $base . '/wp-json/sppc/v1/' . $api_path;

    $default_args = [
        'headers' => [
            'Authorization' => 'Bearer ' . salesloo_get_option('affiliate_promopage_token')
        ]
    ];

    $args = wp_parse_args($args, $default_args);

    $response = wp_remote_get(
        esc_url_raw($url),
        $args
    );

    if (is_wp_error($response)) {
        return (object)[
            'code' => 400,
            'body' => [
                'message' => $response->get_error_message(),
            ]
        ];
    }

    $body = json_decode(wp_remote_retrieve_body($response), true);
    $code = (int) wp_remote_retrieve_response_code($response);

    return (object)[
        'code' => $code,
        'body' => $body
    ];
}

function salesloo_promopage_post($api_path, $args = [])
{
    $base = rtrim(salesloo_get_option('affiliate_promopage_base'), '/');
    $url = $base . '/wp-json/sppc/v1/' . $api_path;

    $default_args = [
        'headers' => [
            'Authorization' => 'Bearer ' . salesloo_get_option('affiliate_promopage_token')
        ]
    ];

    $args = wp_parse_args($args, $default_args);

    $response = wp_remote_post(
        esc_url_raw($url),
        $args
    );

    if (is_wp_error($response)) {
        return (object)[
            'code' => 400,
            'body' => [
                'message' => $response->get_error_message(),
            ]
        ];
    }

    $body = json_decode(wp_remote_retrieve_body($response), true);
    $code = (int) wp_remote_retrieve_response_code($response);

    return (object)[
        'code' => $code,
        'body' => $body
    ];
}
