<?php

namespace Salesloo_Promopage;

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Salesloo_Promopage
 * @subpackage Salesloo_Promopage/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Activator
{

    /**
     * run activate event
     *
     * @since    1.0.0
     */
    public static function run()
    {
    }
}
