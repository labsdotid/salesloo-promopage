<?php

namespace Salesloo_Promopage;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * admin setting classes
 */
class Setting
{
    /**
     * aadd setting section
     *
     * @param  array $sections
     * @return array
     */
    public function add_section($sections)
    {
        $sections['promopage'] = [
            'label' => 'Promopage Generator',
            'callback' => [$this, 'section_view'],
        ];

        return $sections;
    }

    public function section_view()
    {
        \salesloo_field_toggle([
            'label'           => __('Enable', 'salesloo'),
            'name'            => 'affiliate_promopage',
            'description'     => __('Disable or enable affiliate promo page generator', 'salesloo-promopage'),
            'value'   => \salesloo_get_option('affiliate_promopage'),
        ]);

        $note = __('In order to use the promopage site generator you must prepare a website that will be used for the bonus page and install the following plugin %s', 'salesloo-promopage');
        $download_url = 'https://repo.salesloo.com/salesloo-promopage-client.zip';

        \salesloo_field_heading([
            'label' => __('Promo page generator setup', 'salesloo-promopage'),
            'description' => sprintf($note, '<br/> <a target="__blank" href="' . $download_url . '">salesloo-promopage-client.zip</a>'),
            'conditional' => [
                [
                    'field'   => 'affiliate_promopage',
                    'value'   => '1',
                    'compare' => '='
                ]
            ],
        ]);

        \salesloo_field_text([
            'label'           => __('Base url', 'salesloo'),
            'name'            => 'affiliate_promopage_base',
            'description'     => __('Bonus page site generator base url', 'salesloo-promopage'),
            'value'   => \salesloo_get_option('affiliate_promopage_base'),
            'conditional' => [
                [
                    'field'   => 'affiliate_promopage',
                    'value'   => '1',
                    'compare' => '='
                ]
            ],
        ]);

        \salesloo_field_text([
            'label'           => __('Token', 'salesloo'),
            'name'            => 'affiliate_promopage_token',
            'description'     => __('Promo page site generator token', 'salesloo-promopage'),
            'value'   => \salesloo_get_option('affiliate_promopage_token'),
            'conditional' => [
                [
                    'field'   => 'affiliate_promopage',
                    'value'   => '1',
                    'compare' => '='
                ]
            ],
        ]);


        \salesloo_field_submit();
    }

    public function edit_product_setting($product)
    {
        $options = [];
        $promopage_id = salesloo_get_product_meta($product->ID, 'affiliate_promopage', true);

        if ($promopage_id) {

            $res = salesloo_promopage_get('promopage/' . intval($promopage_id));
            $body = $res->body;

            if ($res->code == 200 && isset($body['data']['title'])) {
                $data = isset($body['data']) ? $body['data'] : [];
                $options[intval($promopage_id)] = $data['title'];

                salesloo_update_product_meta($product->ID, 'affiliate_promopage_' . $promopage_id . '_data', $data);
            }
        }

        \salesloo_field_select2([
            'label'       => __('Promo page', 'salesloo-promopage'),
            'name'        => 'affiliate_promopage',
            'description' => '',
            'conditional' => [
                [
                    'field'   => 'affiliate',
                    'value'   => 1,
                    'compare' => '='
                ]
            ],
            'select2_options' => [
                'action' => 'get_promopages',
                'nonce'  => wp_create_nonce('salesloo-ajax')
            ],
            'options' => $options,
            'value' => salesloo_get_product_meta($product->ID, 'affiliate_promopage', true)
        ]);
    }

    /**
     * ajax get all items
     */
    public function ajax_get_promopages()
    {
        $nonce = isset($_REQUEST['nonce']) ? $_REQUEST['nonce'] : '';
        if (!wp_verify_nonce($nonce, 'salesloo-ajax')) exit;

        $paged = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $search = isset($_GET['search']) ? sanitize_text_field($_GET['search']) : '';

        $res = salesloo_promopage_get('promopages');

        $body = $res->body;
        $items = [];

        if ($res->code == 200) {
            $data = isset($body['data']) ? $body['data'] : [];
            foreach ($data as $d) {
                $items[] = [
                    'id' => $d['ID'],
                    'text' => $d['title']
                ];
            }
        } else {
            $items[] = [
                'id' => 0,
                'text' => isset($body['message']) ? sanitize_text_field($body['message']) : 'Error fetch api',
            ];
        }

        $result = [
            'results' => $items,
            'pagination' => [
                'more' => false
            ],
        ];

        echo json_encode($result);
        exit;
    }



    /**
     * constructor
     *
     * @return void
     */
    public function __construct()
    {
    }
}
