<?php

namespace Salesloo_Promopage;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Frontend
 */
class Frontend
{

    public function add_section()
    {

        $promopage_id = salesloo_get_product_meta(___salesloo('product')->ID, 'affiliate_promopage', true);

        if (empty(salesloo_get_option('affiliate_promopage')) || empty($promopage_id)) return;
        ob_start();
?>
        <div class="text-gray-600">
            <div class="flex items-center py-5 border-b border-gray-100 space-x-2">
                <div class="flex-grow">
                    <div class="flex items-center justify-center">
                        <div class="mr-2 relative">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5.882V19.24a1.76 1.76 0 01-3.417.592l-2.147-6.15M18 13a3 3 0 100-6M5.436 13.683A4.001 4.001 0 017 6h1.832c4.1 0 7.625-1.234 9.168-3v14c-1.543-1.766-5.067-3-9.168-3H7a3.988 3.988 0 01-1.564-.317z" />
                            </svg>
                        </div>
                        <h2 class="font-semibold text-lg mr-auto"><?php _e('Promo page', 'salesloo'); ?></h2>
                    </div>
                </div>
                <div class="flex-none">
                    <div class="flex items-center text-blue-800 text-sm cursor-pointer font-semibold space-x-1" @click="$modal('Update Promopage', $refs.generate_bonuspage.innerHTML);">
                        <div class="w-4 h-4">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
                            </svg>
                        </div>
                        <div><?php _e('Generate', 'salesloo'); ?></div>
                    </div>
                    <template x-ref="generate_bonuspage">
                        <div class="relative w-full" x-data>
                            <form method="post">
                                <div class="space-x-4 pb-10">
                                    <div class="flex flex-col space-y-4">
                                        <?php foreach (___salesloo('promopage')['shortcodes'] as $key => $sh) : ?>
                                            <?php
                                            $readonly = '';
                                            $hidden = '';
                                            $type = 'text';

                                            if ($sh['type'] == 'image') {
                                                $type = 'file';
                                            }
                                            if ($key == 'avatar') {
                                                $readonly = ' readonly';
                                                $type = 'text';
                                            }
                                            if (in_array($key, ['pixels', 'link_affiliate'])) {
                                                $hidden = 'hidden';
                                                $type = 'hidden';
                                            }
                                            ?>
                                            <div class="flex-1 <?php echo $hidden; ?>">
                                                <label class="mb-1 text-sm tracking-wide"><?php echo $sh['label']; ?></label>
                                                <div class="relative">
                                                    <input type="<?php echo $type; ?>" name="<?php echo $key; ?>" class="text-base placeholder-gray-300 px-4 rounded-lg border border-gray-200 w-full py-3 focus:outline-none focus:border-blue-900" value="<?php echo $sh['value']; ?>" <?php echo $readonly; ?> />
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="flex flex-row-reverse mt-5">
                                        <div class="w-1/2">
                                            <button type="submit" class="text-sm font-bold border rounded-lg text-white px-5 py-4 bg-blue-800 hover:bg-blue-700 flex items-center justify-center space-x-2 w-full">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                                <span><?php _e('Generate', 'salesloo'); ?></span>
                                            </button>
                                        </div>
                                        <?php salesloo_nonce_field('generate_promopage'); ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </template>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-sm">
                <div class="flex flex-col">
                    <div class="w-full px-5 sm:px-10">
                        <template x-for="[key, val] in Object.entries(promopages)">
                            <div class="relative border-b border-dashed py-10">
                                <div class="flex space-x-2">
                                    <div class="flex-grow">
                                        <div class="flex text-blue-900 font-bold items-center">
                                            <svg class="w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8l4 4m0 0l-4 4m4-4H3" />
                                            </svg>
                                            <a target="__blank" x-bind:href="val.link" x-text="val.title"></a>
                                        </div>
                                        <div class="block overflow-hidden">
                                            <input class="w-full p-0 pr-5 text-sm text-gray-400 border-none focus:outline-none focus:ring-0" type="text" readonly x-model="val.link" />
                                        </div>
                                    </div>
                                    <div class="flex-none">
                                        <div class="flex items-center justify-center h-full space-x-2">
                                            <div class="flex-none w-14" @click="$copy(val.link)">
                                                <div class="flex items-center leading-lg px-1 whitespace-no-wrap text-green-700 text-sm cursor-pointer font-bold">
                                                    <div class="w-4 h-4">
                                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7v8a2 2 0 002 2h6M8 7V5a2 2 0 012-2h4.586a1 1 0 01.707.293l4.414 4.414a1 1 0 01.293.707V15a2 2 0 01-2 2h-2M8 7H6a2 2 0 00-2 2v10a2 2 0 002 2h8a2 2 0 002-2v-2" />
                                                        </svg>
                                                    </div>
                                                    <span class="ml-1"><?php _e('Copy', 'salesloo'); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </template>
                        <template x-if="Object.entries(promopages).length < 1">
                            <div class="relative border-b border-dashed py-10">
                                <div class="flex space-x-2">
                                    <div class="flex-grow">
                                        <div class="block overflow-hidden text-sm text-gray-400">
                                            <?php _e('Your promotion page is not yet available, please click generate to make it', 'salesloo'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </template>
                    </div>
                </div>
            </div>
        </div>

<?php
        echo ob_get_clean();
    }

    public function action($request, $frontend)
    {
        global $___salesloo;

        if (isset($_POST['generate_promopage']) && salesloo_verify_nonce($_POST['generate_promopage'])) {

            $promopage_id = salesloo_get_product_meta($frontend->product_id, 'affiliate_promopage', true);
            $promopage_data = salesloo_get_product_meta($frontend->product_id, 'affiliate_promopage_' . $promopage_id . '_data', true);

            if (isset($promopage_data['shortcodes'])) {
                $promopage_shortcodes = $promopage_data['shortcodes'];

                foreach ($promopage_shortcodes as $key => $sh) {

                    if (isset($_FILES[$key]) && $sh['type'] == 'image') {
                        $file = $_FILES[$key];
                        $type = $file['type'];
                        $extensions = array('image/jpeg', 'image/png', 'image/gif');
                        if (in_array($type, $extensions)) {
                            $upload = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));
                            $sh['value'] = esc_url_raw($upload['url']);
                        }
                    }

                    if (isset($_POST[$key])) {
                        if ($sh['type'] == 'image') {
                            $sh['value'] = esc_url_raw($_POST[$key]);
                        } else {
                            $sh['value'] = sanitize_text_field($_POST[$key]);
                        }
                    }
                    $promopage_shortcodes[$key] = $sh;
                }

                salesloo_update_product_meta($frontend->product_id, 'affiliate_promopage_' . get_current_user_id(), $promopage_shortcodes);

                $res = salesloo_promopage_post(
                    'promopage/' . intval($promopage_id),
                    [
                        'body' => [
                            'shortcodes' => $promopage_shortcodes,
                            'username' => wp_get_current_user()->user_login
                        ]
                    ]
                );

                $body = $res->body;

                if ($res->code == 200) {
                    $pages = isset($body['data']['pages']) ? $body['data']['pages'] : [];
                    salesloo_update_product_meta($frontend->product_id, 'affiliate_promopage_' . get_current_user_id() . '_pages', $pages);
                } else {
                    $___salesloo['alert'] = [
                        'type' => 'danger',
                        'message' => isset($body['message']) ? $body['message'] : 'Error',
                    ];
                }
            }
        }
    }

    public function load($frontend)
    {
        global $___salesloo;

        if (empty(salesloo_get_option('affiliate_promopage'))) return;

        $promopage_id = salesloo_get_product_meta(___salesloo('product')->ID, 'affiliate_promopage', true);
        $promopage_data = salesloo_get_product_meta(___salesloo('product')->ID, 'affiliate_promopage_' . $promopage_id . '_data', true);

        $shortcodes = [];

        if (isset($promopage_data['shortcodes'])) {
            $promopage_shortcodes = $promopage_data['shortcodes'];

            foreach ($promopage_shortcodes as $key => $sh) {
                $sh['value'] = '';
                $promopage_shortcodes[$key] = $sh;
            }

            $custom_shortcodes = salesloo_get_product_meta(___salesloo('product')->ID, 'affiliate_promopage_' . get_current_user_id(), true);
            if (empty($custom_shortcodes)) {
                $custom_shortcodes = [
                    'avatar' => [
                        'label' => __('Avatar', 'salesloo-promopage'),
                        'type' => 'image',
                        'value' => get_user_meta(get_current_user_id(), 'avatar', true),
                        'default' => true
                    ],
                    'first_name' => [
                        'label' => __('First Name', 'salesloo-promopage'),
                        'type' => 'text',
                        'value' => wp_get_current_user()->first_name,
                        'default' => true
                    ],
                    'last_name' => [
                        'label' => __('Last Name', 'salesloo-promopage'),
                        'type' => 'text',
                        'value' => wp_get_current_user()->last_name,
                        'default' => true
                    ],
                    'pixels' => [
                        'label' => __('Facebook Pixel Ids', 'salesloo-promopage'),
                        'type' => 'fb_pixel_id',
                        'value' => salesloo_get_product_meta(___salesloo('product')->ID, 'fbpixel_' . get_current_user_id(), true),
                        'default' => true
                    ],
                    'link_affiliate' => [
                        'label' => __('Link Affiliate', 'salesloo-promopage'),
                        'type' => 'link_affiliate',
                        'value' => salesloo_url_checkout(___salesloo('product')->slug),
                        'default' => true
                    ]
                ];
            }

            $shortcodes = wp_parse_args($custom_shortcodes, $promopage_shortcodes);
        }

        $___salesloo['promopage'] = [
            'shortcodes' => $shortcodes,
        ];

        $promopage_pages = salesloo_get_product_meta($frontend->product_id, 'affiliate_promopage_' . get_current_user_id() . '_pages', true);

        $pages = [];
        $base = rtrim(salesloo_get_option('affiliate_promopage_base'), '/');

        foreach ((array)$promopage_pages as $p) {
            $path = ltrim($p['path'], '/');
            $p['link'] = $base . '/' . $path;
            $pages[] = $p;
        }

        $___salesloo['data']['promopages'] = $pages;
    }



    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
    }
}
