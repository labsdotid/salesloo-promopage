<?php

namespace Salesloo_Promopage;

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Salesloo_AB
 * @subpackage Salesloo_AB/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Deactivator
{

    /**
     * run deactivator
     *
     * @since    1.0.0
     */
    public static function run()
    {
    }
}
