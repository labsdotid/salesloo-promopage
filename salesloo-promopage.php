<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.fiqhidayat.com
 * @since             1.0.0
 * @package           Salesloo_Promopage
 *
 * @wordpress-plugin
 * Plugin Name:       Salesloo Promopage
 * Plugin URI:        https://www.salesloo.com
 * Description:       Make it easy for affiliates to promote your product
 * Version:           1.0.4
 * Author:            Salesloo
 * Author URI:        https://www.salesloo.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       salesloo-promopage
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('SALESLOO_PROMOPAGE_VERSION', '1.0.4');
define('SALESLOO_PROMOPAGE_URL', plugin_dir_url(__FILE__));
define('SALESLOO_PROMOPAGE_PATH', plugin_dir_path(__FILE__));
define('SALESLOO_PROMOPAGE_ROOT', __FILE__);

require 'update-checker/plugin-update-checker.php';

/**
 * The code that runs during plugin activation.
 */
function activate_salesloo_promopage()
{
    require_once SALESLOO_PROMOPAGE_PATH . 'includes/activator.php';
    Salesloo_Promopage\Activator::run();
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_salesloo_promopage()
{
    require_once SALESLOO_PROMOPAGE_PATH . 'includes/deactivator.php';
    Salesloo_Promopage\Deactivator::run();
}

register_activation_hook(__FILE__, 'activate_salesloo_promopage');
register_deactivation_hook(__FILE__, 'deactivate_salesloo_promopage');

/**
 * Main salesloo affiliate bonus class
 */
class Salesloo_Promopage
{
    /**
     * Plugin setting
     */
    private $_setting;

    private $_frontend;

    /**
     * Instance
     */
    private static $_instance = null;

    /**
     * run
     *
     * @return Salesloo_Promopage An instance of class
     */
    public static function run()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'on_plugins_loaded']);
    }

    /**
     * Load Textdomain
     *
     * Load plugin localization files.
     *
     * Fired by `init` action hook.
     *
     * @access public
     */
    public function i18n()
    {
        load_plugin_textdomain(
            'salesloo-promopage',
            false,
            dirname(plugin_basename(__FILE__)) . '/languages'
        );
    }

    /**
     * On Plugins Loaded
     *
     * Checks if Salesloo has loaded, and performs some compatibility checks.
     *
     * @access public
     */
    public function on_plugins_loaded()
    {

        if ($this->is_compatible()) {
            $this->i18n();

            $this->load_dependencies();

            add_filter('salesloo/admin/settings/affiliate/sections', [$this->_setting, 'add_section']);
            add_action('salesloo/admin/product/edit/affiliate/after', [$this->_setting, 'edit_product_setting']);
            add_action('wp_ajax_get_promopages', [$this->_setting, 'ajax_get_promopages']);

            add_action('salesloo/affiliate/product/end', [$this->_frontend, 'add_section']);
            add_action('salesloo/frontend/affiliate/product/id/load', [$this->_frontend, 'load']);
            add_action('salesloo/frontend/affiliate/product/id/action', [$this->_frontend, 'action'], 10, 2);
        }
    }

    public function load_dependencies()
    {
        require_once SALESLOO_PROMOPAGE_PATH . 'includes/setting.php';
        require_once SALESLOO_PROMOPAGE_PATH . 'includes/frontend.php';
        require_once SALESLOO_PROMOPAGE_PATH . 'includes/function.php';

        $this->_setting = new \Salesloo_Promopage\Setting;
        $this->_frontend = new \Salesloo_Promopage\Frontend;
    }


    /**
     * Compatibility Checks
     *
     * @access public
     */
    public function is_compatible()
    {
        // Check if Salesloo installed and activated
        if (!did_action('salesloo/loaded')) {
            add_action('admin_notices', [$this, 'admin_notice_missing_main_plugin']);
            return false;
        }

        return true;
    }


    /**
     * Admin notice
     *
     * Warning when the site doesn't have Salesloo installed or activated.
     *
     * @access public
     */
    public function admin_notice_missing_main_plugin()
    {

        if (isset($_GET['activate'])) unset($_GET['activate']);

        $message = sprintf(
            /* translators: 1: Salesloo Affiliate Bonus 2: Salesloo */
            esc_html__('"%1$s" requires "%2$s" to be installed and activated.', 'salesloo-ab'),
            '<strong>' . esc_html__('Salesloo Affiliate Bonus', 'salesloo-ab') . '</strong>',
            '<strong>' . esc_html__('Salesloo', 'salesloo-ab') . '</strong>'
        );

        printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
    }
}

Salesloo_Promopage::run();
